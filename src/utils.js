import React from "react";

export const API_URL = "https://api.coingecko.com/api/v3"; //api root url

export const handleResponse = response => {
  return response.json().then(json => {
    return response.ok ? json : Promise.reject(json);
  });
};

export function renderPercent(percent, icon = true) {
  const number = parseFloat(percent);
  if (number > 0)
    return (
      <>
        <span className="text-success mr-2">{icon ? "↑" : "up"}</span>
        <span className="text-success text-nowrap">{percent}</span>
      </>
    );
  else if (number < 0)
    return (
      <>
        <span className="text-danger mr-2">{icon ? "↓" : "down"}</span>
        <span className="text-danger text-nowrap">{percent}</span>
      </>
    );
  return <span>{percent}</span>;
}

export const formatPercentage = percent =>
  Intl.NumberFormat(undefined, {
    style: "percent",
    minimumFractionDigits: 3
  }).format(percent / 100);

export const formatUSD = currency =>
  Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "usd",
    minimumFractionDigits: 0
  }).format(currency);

export function numberToWord(number) {
  const ONE_MILLION = 1000000;
  const ONE_BILLION = 1000000000; //         1.000.000.000 (9)
  const ONE_TRILLION = 1000000000000; //     1.000.000.000.000 (12)
  const ONE_QUADRILLION = 1000000000000000; // 1.000.000.000.000.000 (15)
  const MAX = 9007199254740992; // 9.007.199.254.740.992 (15)

  if (number < ONE_MILLION) return number;
  else if (number < ONE_BILLION)
    return Math.floor(number / ONE_MILLION) + " Million";
  else if (number < ONE_TRILLION)
    return Math.floor(number / ONE_BILLION) + " Billion";
  else if (number < ONE_QUADRILLION)
    return Math.floor(number / ONE_TRILLION) + " Trillion";
  else if (number <= MAX)
    return Math.floor(number / ONE_QUADRILLION) + " Quadrillion";

  return "∞";
}

export const getDomain = url =>
  url
    ? url
        .replace("http://", "")
        .replace("https://", "")
        .split(/[/?#]/)[0]
    : "";
