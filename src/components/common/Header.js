import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Icon from "../icons/Icon";

function Header() {
  return (
    <Navbar
      bg="dark"
      variant="dark"
      sticky="top"
      className="header"
      style={{ boxShadow: "inset 0 0 70px -20px" }}
    >
      <div className="navbar-wrapper">
        <Navbar.Brand href="#home">
          <Icon name="logo" width="64" /> React Coin
        </Navbar.Brand>
      </div>
    </Navbar>
  );
}

export default Header;
