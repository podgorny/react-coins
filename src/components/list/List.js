import React, { useState, useEffect } from "react";
import Spinner from "react-bootstrap/Spinner";
import Table from "react-bootstrap/Table";
import { handleResponse, API_URL } from "../../utils";
import ListItem from "./ListItem";
import Alert from "react-bootstrap/Alert";
import Container from "react-bootstrap/Container";
import ListPagination from "./ListPagination";
import ItemDetail from "./ItemDetail";

function List() {
  const [loading, setloading] = useState(false);
  const [currencies, setcurrencies] = useState([]);
  const [error, seterror] = useState(null);
  const [activePage, setActivePage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [detailModalShow, setDetailModalShow] = useState(false);
  const [detailCoinId, setDetailCoinId] = useState("");
  const pageLimit = 100; // currencies per page; max = 250

  useEffect(() => {
    setloading(true);

    fetch(
      `${API_URL}/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=${pageLimit}&page=${activePage}&sparkline=false&price_change_percentage=1h%2C24h%2C7d`
    )
      .then(handleResponse)
      .then(data => {
        setcurrencies(data);
        setloading(false);
      })
      .catch(error => {
        seterror(error);
        setloading(false);
      });
  }, [activePage]);

  const getCoinId = id => {
    setDetailModalShow(true);
    setDetailCoinId(id);
  };

  if (loading) {
    return (
      <div className="d-flex justify-content-center p-4">
        <Spinner animation="grow" variant="light" />
      </div>
    );
  } else if (error) {
    return (
      <div className="d-flex justify-content-center p-4">
        <Alert variant="danger" show={error}>
          <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
          <p>{error}</p>
        </Alert>
      </div>
    );
  } else
    return (
      <Container
        className="mt-4 p-0 rounded-lg"
        style={{
          boxShadow: "15px 0px 30px 0px rgba(255,255,255,0.1)",
          borderRadius: 10
        }}
      >
        <Table
          striped
          borderless
          variant="dark"
          responsive
          hover
          className="rounded-lg"
        >
          <thead>
            <tr className="table-active">
              <th className="text-center">#</th>
              <th width="300px">Coin</th>
              <th>Price</th>
              <th>1h</th>
              <th>24h</th>
              <th>7d</th>
              <th>Mkt Cap</th>
            </tr>
          </thead>
          <tbody>
            {currencies.map(currency => (
              <ListItem
                key={currency.id}
                currency={currency}
                getCoinId={getCoinId}
              />
            ))}
          </tbody>
        </Table>
        <ListPagination
          pageInfo={{
            activePage,
            setActivePage,
            pageLimit,
            totalPages,
            setTotalPages
          }}
        />

        <ItemDetail
          onHide={() => setDetailModalShow(false)}
          show={detailModalShow}
          detailCoinId={detailCoinId}
        />
      </Container>
    );
}

export default List;
