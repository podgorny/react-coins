import React from "react";
import PropTypes from "prop-types";
import { formatPercentage, formatUSD, renderPercent } from "../../utils";

function ListItem({ currency, getCoinId }) {
  const {
    market_cap_rank,
    image,
    name,
    current_price,
    price_change_percentage_24h,
    market_cap,
    symbol,
    price_change_percentage_1h_in_currency,
    price_change_percentage_7d_in_currency
  } = currency;

  return (
    <tr>
      <td className="text-center">{market_cap_rank}</td>
      <td
        onClick={() => getCoinId(currency.id)}
        className="list__currency-brand"
      >
        <div className="d-flex align-items-center justify-content-between">
          <div className="d-flex align-items-center mr-1">
            <img src={image} alt={name} width="32" className="mr-2" />
            <strong>{name}</strong>
          </div>
          <sub className="text-uppercase font-weight-light text-white-50">
            {symbol}
          </sub>
        </div>
      </td>
      <td>$ {current_price}</td>
      <td>
        {renderPercent(
          formatPercentage(price_change_percentage_1h_in_currency)
        )}
      </td>
      <td>{renderPercent(formatPercentage(price_change_percentage_24h))}</td>
      <td>
        {renderPercent(
          formatPercentage(price_change_percentage_7d_in_currency)
        )}
      </td>
      <td>{formatUSD(market_cap)}</td>
    </tr>
  );
}

ListItem.propTypes = {
  currency: PropTypes.object.isRequired
};

export default ListItem;
