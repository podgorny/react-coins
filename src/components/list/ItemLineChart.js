import React, { useEffect, useState } from "react";
import { handleResponse, API_URL } from "../../utils";
import Spinner from "react-bootstrap/Spinner";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer
} from "recharts";

function ItemLineChart({ data }) {
  const [isLoading, setIsLoading] = useState(false);

  return isLoading ? (
    <div className="d-flex justify-content-center p-4">
      <Spinner animation="grow" />
    </div>
  ) : (
    // <ResponsiveContainer width={700} height="80%">
    <LineChart
      width={600}
      height={300}
      data={data.prices}
      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
    >
      <defs>
        <linearGradient id="colorUv" x1="0%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" stopColor="green" />
          <stop offset="100%" stopColor="red" />
        </linearGradient>
      </defs>
      <CartesianGrid vertical={false} strokeWidth={0.5} />
      <XAxis dataKey="date" domain={["dataMin", "dataMax"]} />
      <YAxis domain={["dataMin", "dataMax"]} tickCount={7} />
      <Tooltip />
      <Line
        strokeWidth={1.5}
        type="monotone"
        dataKey="value"
        stroke="url(#colorUv)"
        activeDot={{ r: 3 }}
        dot={false}
      />
    </LineChart>
    // </ResponsiveContainer>
  );
}

export default ItemLineChart;
