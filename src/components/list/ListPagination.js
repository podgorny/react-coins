import React, { useEffect } from "react";
import Pagination from "react-bootstrap/Pagination";
import { handleResponse, API_URL } from "../../utils";
import PropTypes from "prop-types";

function ListPagination({
  pageInfo: { activePage, setActivePage, pageLimit, setTotalPages, totalPages }
}) {
  const LEFT_PAGE = "LEFT";
  const RIGHT_PAGE = "RIGHT";
  const pageNeighbours = 1; // pages numbers to left and to right of the active one

  useEffect(() => {
    if (totalPages < 2) {
      getTotalPages();
    }
  }, []);

  const getTotalPages = () => {
    fetch(`${API_URL}/coins/list`)
      .then(handleResponse)
      .then(data => {
        setTotalPages(prev => Math.ceil(data.length / pageLimit));
      })
      .catch(error => {
        console.error(error);
      });
  };

  const gotoPage = page => {
    const currentPage = Math.max(0, Math.min(page, totalPages));

    setActivePage(currentPage);
  };

  const handleClick = page => evt => {
    evt.preventDefault();
    gotoPage(page);
  };

  const handleMoveLeft = evt => {
    evt.preventDefault();
    gotoPage(activePage - pageNeighbours * 2 - 1);
  };

  const handleMoveRight = evt => {
    evt.preventDefault();
    gotoPage(activePage + pageNeighbours * 2 + 1);
  };

  /**
   * Helper method for creating a range of numbers
   * range(1, 5) => [1, 2, 3, 4, 5]
   */
  const getRange = (from, to, step = 1) => {
    let i = from;
    const range = [];

    while (i <= to) {
      range.push(i);
      i += step;
    }

    return range;
  };

  /**
   * Let's say we have 10 pages and we set pageNeighbours to 2
   * Given that the current page is 6
   * The pagination control will look like the following:
   *
   * (1) < {4 5} [6] {7 8} > (10)
   *
   * (x) => terminal pages: first and last page(always visible)
   * [x] => represents current page
   * {...x} => represents page neighbours
   */

  const fetchPageNumbers = () => {
    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(...) and right(...) controls
     */
    const totalNumbers = pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, activePage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, activePage + pageNeighbours);

      let pages = getRange(startPage, endPage);

      /**
       * hasLeftSpill: has hidden pages to the left
       * hasRightSpill: has hidden pages to the right
       * spillOffset: number of hidden pages either to the left or to the right
       */
      const hasLeftSpill = startPage > 2;
      const hasRightSpill = totalPages - endPage > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      // handle: (1) < {5 6} [7] {8 9} (10)
      if (hasLeftSpill && !hasRightSpill) {
        const extraPages = getRange(startPage - spillOffset, startPage - 1);
        pages = [LEFT_PAGE, ...extraPages, ...pages];
      }

      // handle: (1) {2 3} [4] {5 6} > (10)
      else if (!hasLeftSpill && hasRightSpill) {
        const extraPages = getRange(endPage + 1, endPage + spillOffset);
        pages = [...pages, ...extraPages, RIGHT_PAGE];
      }

      // handle: (1) < {4 5} [6] {7 8} > (10)
      else {
        pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
      }

      return [1, ...pages, totalPages];
    }

    return getRange(1, totalPages);
  };

  const pages = fetchPageNumbers();

  return !totalPages || totalPages === 1 ? null : (
    <Pagination className="pagination justify-content-center py-3">
      {pages.map((page, i) => {
        if (page === LEFT_PAGE)
          return (
            <Pagination.Ellipsis
              key={i}
              aria-label="previous jump"
              onClick={handleMoveLeft}
            />
          );
        if (page === RIGHT_PAGE)
          return (
            <Pagination.Ellipsis
              key={i}
              aria-label="next jump"
              onClick={handleMoveRight}
            />
          );

        return (
          <Pagination.Item
            key={i}
            className={activePage === page ? "active" : ""}
            onClick={handleClick(page)}
          >
            {page}
          </Pagination.Item>
        );
      })}
    </Pagination>
  );
}

ListPagination.propTypes = {
  pageInfo: PropTypes.shape({
    activePage: PropTypes.number,
    setActivePage: PropTypes.func,
    pageLimit: PropTypes.number,
    setTotalPages: PropTypes.func,
    totalPages: PropTypes.number
  }).isRequired
};

export default ListPagination;
