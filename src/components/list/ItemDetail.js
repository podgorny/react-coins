import React, { useEffect, useState, lazy, Suspense } from "react";
import Modal from "react-bootstrap/Modal";
import Spinner from "react-bootstrap/Spinner";
import Badge from "react-bootstrap/Badge";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import {
  handleResponse,
  API_URL,
  formatUSD,
  renderPercent,
  formatPercentage,
  numberToWord,
  getDomain
} from "../../utils";
import ItemLineChart from "./ItemLineChart";

// const ItemLineChart = lazy(() => import("./ItemLineChart"));

function ItemDetail({ onHide, show, detailCoinId }) {
  const [currencyInfo, setcurrencyInfo] = useState({
    image: { large: "https://img.icons8.com/cotton/2x/bitcoin.png" },
    categories: [],
    market_data: {
      current_price: {},
      total_volume: {},
      market_cap: {},
      high_24h: {},
      low_24h: {}
    },
    links: { blockchain_site: [], homepage: [] }
  });
  const [isLoading, setIsLoading] = useState(false);
  const [key, setKey] = useState("general");
  const [chartData, setChartData] = useState([]);

  useEffect(() => {
    setKey("general");
    setIsLoading(true);

    if (detailCoinId) {
      fetch(`${API_URL}/coins/${detailCoinId}?localization=false&tickers=false`)
        .then(handleResponse)
        .then(data => {
          console.log(data);
          setcurrencyInfo(data);
          setIsLoading(false);
        })
        .catch(error => {
          console.error(error);
        });

      fetch(
        `${API_URL}/coins/${detailCoinId}/market_chart?vs_currency=usd&days=1`
      )
        .then(handleResponse)
        .then(data => {
          console.log(normalizeData(data));
          setChartData(normalizeData(data));
        })
        .catch(error => {
          console.error(error);
        });
    }
  }, [detailCoinId]);

  const normalizeData = data => {
    const dateOptions = {
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "2-digit",
      second: "2-digit"
    };
    const normalizedData = Object.fromEntries(
      Object.entries(data).map(arr => [
        arr[0],
        arr[1].map(dataArr => ({
          date: new Date(dataArr[0]).toLocaleDateString("en-US", dateOptions),
          value: dataArr[1]
        }))
      ])
    );
    return normalizedData;
  };

  const {
    name,
    symbol,
    image,
    categories,
    hashing_algorithm,
    market_data: {
      current_price,
      total_volume,
      price_change_percentage_24h,
      total_supply,
      circulating_supply,
      market_cap,
      market_cap_rank,
      high_24h,
      low_24h
    },
    links: { blockchain_site, homepage }
  } = currencyInfo;

  return (
    <Modal
      onHide={onHide}
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      {isLoading ? (
        <div className="d-flex justify-content-center p-4">
          <Spinner animation="grow" />
        </div>
      ) : (
        <>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              <div className="d-flex">
                <img src={image.large} width="64" alt="coin-logo" />
                <div className="ml-2 d-flex flex-column align-items-center">
                  <span>{name}</span>
                  <span className="text-monospace text-uppercase text-white-50 small">
                    {symbol}
                  </span>
                </div>
              </div>
            </Modal.Title>
          </Modal.Header>
          <Tabs activeKey={key} onSelect={k => setKey(k)}>
            <Tab eventKey="general" title="General">
              <Modal.Body>
                <div className="mb-2">
                  {categories.map(category => (
                    <Badge
                      pill
                      variant="secondary"
                      className="mr-2"
                      key={category}
                    >
                      {category}
                    </Badge>
                  ))}
                </div>
                <p>
                  <em>{name}</em> price today is
                  <span className="font-weight-bolder">
                    {" "}
                    ${current_price.usd}{" "}
                  </span>
                  with a 24-hour trading volume of {formatUSD(total_volume.usd)}
                  . <em className="text-monospace text-uppercase">{symbol}</em>{" "}
                  price is{" "}
                  {renderPercent(
                    formatPercentage(price_change_percentage_24h),
                    false
                  )}{" "}
                  in the last 24 hours. It has a circulating supply of{" "}
                  {numberToWord(circulating_supply)} coins and a max supply of{" "}
                  {numberToWord(total_supply) || "∞"} coins. In order to explore
                  addresses and transactions, you may use block explorers such
                  as{" "}
                  <a
                    href={blockchain_site[0]}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {getDomain(blockchain_site[0])}
                  </a>
                  . Additional information can be found at{" "}
                  <a
                    href={homepage[0]}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {getDomain(homepage[0])}
                  </a>
                  .
                </p>
              </Modal.Body>
            </Tab>
            <Tab eventKey="stats" title="Quick Stats">
              <Modal.Body className="row">
                <dl className="d-flex flex-column quick-stats-list col-xl-7 col-md-10 col-lg-7 col-sm-12">
                  <div className="quick-stats-list__info">
                    <dt>{name} Price</dt>
                    <dd>${current_price.usd}</dd>
                  </div>
                  <div className="quick-stats-list__info">
                    <dt>Market Cap</dt>
                    <dd>{formatUSD(market_cap.usd)}</dd>
                  </div>
                  <div className="quick-stats-list__info">
                    <dt>Trading Volume </dt>
                    <dd>{formatUSD(total_volume.usd)}</dd>
                  </div>
                  <div className="quick-stats-list__info">
                    <dt>24h Low / 24h High</dt>
                    <dd>
                      ${low_24h.usd} / ${high_24h.usd}
                    </dd>
                  </div>
                  <div className="quick-stats-list__info">
                    <dt>Market Cap Rank</dt>
                    <dd>#{market_cap_rank}</dd>
                  </div>
                  <div className="quick-stats-list__info">
                    <dt>Hashing Algorithm</dt>
                    <dd>{hashing_algorithm || "🔒"}</dd>
                  </div>
                  <div className="quick-stats-list__info">
                    <dt>Official page</dt>
                    <dd>
                      {" "}
                      <a
                        href={homepage[0]}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {homepage[0]}
                      </a>
                    </dd>
                  </div>
                </dl>
              </Modal.Body>
            </Tab>
            <Tab eventKey="chart" title="Chart">
              <Modal.Body>
                {/* <Suspense
                  fallback={
                    <div className="d-flex justify-content-center p-4">
                      <Spinner animation="grow" />
                    </div>
                  }
                >
                  {key === "chart" && <ItemLineChart id={detailCoinId} />} */}
                <ItemLineChart data={chartData} />
                {/* </Suspense> */}
              </Modal.Body>
            </Tab>
          </Tabs>
        </>
      )}
    </Modal>
  );
}

export default ItemDetail;
