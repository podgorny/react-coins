import React from "react";
import Logo from "./Logo";

const Icon = ({ name, width }) => {
  switch (name) {
    case "logo":
      return <Logo width={width} />;

    default:
  }
};

export default Icon;
