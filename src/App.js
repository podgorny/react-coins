import React from "react";
import Header from "./components/common/Header";
import List from "./components/list/List";

function App() {
  return (
    <>
      <Header />
      <List />
    </>
  );
}

export default App;
